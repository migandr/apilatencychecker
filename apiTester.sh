#!/bin/bash

version=0.2.0

printhelp(){
  echo "API Latency checked"
  echo "version " $version
  echo ""
  echo ""
  echo "-u: url to make the API call"
  echo "-i: number of API calls for thread"
  echo "-t: number of parallel threads"
  echo "-v: version"
  echo "-h: help"
  exit 1
}

starter(){
  echo "Start testing on: " $url
  echo "Number of simultaneous users: " $threads
  echo "Number of API Calls: " $iterator
}

while getopts t:u:i:vhH: option
do
case "${option}" in
  u ) url=${OPTARG}; if [ ${url:0:1} == '-' ];then echo "error"; exit 1; fi	;;
  i ) iterator=${OPTARG}			;;
  v ) echo "version " $version; exit 1		;;
  t ) threads=${OPTARG}				;;
  H ) headers=${OPTARG}				;;
  h ) printhelp					;;
  ? ) echo "Invalid"; exit 1			;;
esac
done

## Checkers for flag arguments:

if [ -z $url ];
then
  echo "URL is a required parameter"
  exit 1
fi

if [ -z $iterator ];
then
  iterator=25
fi

## Starting script

executor(){
  for ((i=0; i < $1; i++));
  do
    /usr/bin/time -f "%e" curl -s $2 2>> timexec.txt > /dev/null &
    wait
  done
}


executorLong(){
  for ((i=0; i < $1; i++));
  do
    /usr/bin/time -f "%e" curl -s $2 -H $2 2>> timexec.txt > /dev/null &
    wait
  done
}



if [ -z $header ];
starter
then
  for ((i=0; i < $threads; i++));
    do
    executor $iterator $url &
    wait
  done
else
  for ((i=0; i < $threads; i++));
    do
    executorLong $iterator $url $header &
    wait
  done
fi

input="timexec.txt"
result=0
while IFS= read -r line;
do
  result=$(bc -l <<<"${result}+${line}")
done < "$input"

divider=$(bc -l <<< ${iterator}*${threads})
echo $result "/" $divider
echo $(bc -l <<< ${result}/${divider})

$(rm -f "timexec.txt")
