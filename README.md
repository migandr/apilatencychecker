# apiLatencyChecker

Bash script that recibes some parameters and returns the average response time from an API



### Sintaxis:

./apiTester.sh -u[MANDATORY] -i[OPTIONAL] -t[OPTIONAL] 

-u  url for the request

-i  number of iterations per thread, if empty 25

-t  number of threads in parallel

-H  authorization headers in format "Authorization: code"

./apiTester.sh -h

-h  help

./apiTester.sh -v

-v  version



### Next Steps:

Threads control, if empty by default 4.

Add if CURL must be under ssl certificate.

Add if the user wants to save the history and the name of the file where it wants.
